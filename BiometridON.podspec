Pod::Spec.new do |s|
s.name              = 'BiometridON'
s.version           = '1.0.3-dev'
s.summary           = 'BiometridOn is a service-based onboarding solution'
s.homepage          = 'https://polygon.pt/'

s.author            = { 'Name' => 'Polygon' }
s.license           = { :type => 'MIT', :file => 'LICENSE' }

s.platform          = :ios
s.source            = { :http => 'https://bitbucket.org/polygon_/biometridon.git' }

s.ios.deployment_target = '10.0'
s.ios.vendored_frameworks = 'BiometridOn.framework', 'ZoomAuthenticationHybrid.framework'

end
