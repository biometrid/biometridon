//
//  BiometridOn.h
//  BiometridOn
//
//  Created by Tiago Carvalho on 17/01/2018.
//  Copyright © 2018 Tiago Carvalho. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BiometridOnSDK.h"
#import "BiometridOnManager.h"
#import "BiometridOnEnums.h"
#import "BiometridOnCamera.h"
#import "BiometridOnHelper.h"
#import "BiometridOnLiveness.h"
#import "BiometridOnLivenessCustomization.h"

//! Project version number for BiometridOn.
FOUNDATION_EXPORT double BiometridOnVersionNumber;

//! Project version string for BiometridOn.
FOUNDATION_EXPORT const unsigned char BiometridOnVersionString[];



