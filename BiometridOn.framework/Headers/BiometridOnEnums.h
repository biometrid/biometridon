//
//  BiometridOnEnums.h
//  BiometridOn
//
//  Created by Tiago Carvalho on 22/01/2018.
//  Copyright © 2018 Tiago Carvalho. All rights reserved.
//

#ifndef BiometridOnEnums_h
#define BiometridOnEnums_h

/*!
 * @typedef Orientation
 * @brief Position of the object (document).
 * @constant BACK Referring to back side.
 * @constant FRONT Referring to front side.
 */
typedef NS_ENUM(NSInteger, Orientation) {
    
    BACK = 1,
    FRONT

};

/*!
 * @typedef StepType
 * @brief Step types available in the process.
 * @constant FILES Step to submit files (document photos).
 * @constant FORM_FILES Step to submit form data and files.
 * @constant MULTI_FILES Step to submit multiple files.
 * @constant FORM Step to submit form data.
 * @constant IDCARD Step to submit an ID card.
 * @constant SELFIE Step to take a selfie.
 * @constant VIDEOCONFERENCE Step to start a video conference.
 */
typedef NS_ENUM(NSInteger, StepType) {
    
    FILES = 1,
    FORM,
    FORM_FILES,
    MULTI_FILES,
    IDCARD,
    SELFIE,
    VIDEOCONFERENCE,
    OCR_SELFIE,
    FINISHED
    
};

/*!
 * @typedef CancelBtnLoc
 * @brief Button locations available.
 * @constant LEFT Cancel Button will be at left side.
 * @constant RIGHT Cancel Button will be at right side.
 * @constant DISABLED Cancel Button will dissapears.
 */
typedef NS_ENUM(NSUInteger, CancelBtnLoc) {
    
    LEFT,
    RIGHT,
    DISABLED
};

#endif /* BiometridOnEnums_h */
