//
//  BiometridOnLivenessCustomization.h
//  BiometridOn
//
//  Created by André Santana on 02/05/2019.
//  Copyright © 2019 Tiago Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BiometridOnEnums.h"

@interface BiometridOnLivenessCustomization : NSObject

+ (instancetype)sharedInstance;

/*!
 *  @discussion Custom branding logo.
 *  @param image of brandingLogo.
 */
- (void)setCustomizationBrandingLogo:(UIImage*)image;

/*!
 *  @discussion Progress color one.
 *  @param color option.
 */
- (void)setOvalCustomizationProgressColorOne:(UIColor*)color;

/*!
 *  @discussion Progress color two.
 *  @param color option.
 */
- (void)setOvalCustomizationProgressColorTwo:(UIColor*)color;

/*!
 *  @discussion Feedback background color.
 *  @param array of gradient.
 */
- (void)setFeedbackCustomizationBackgroundColor:(NSArray*)array;

/*!
 *  @discussion Frame customization background color.
 *  @param color of background color.
 */
- (void)setFrameCustomizationBackgroundColor:(UIColor*)color;

/*!
 *  @discussion Cancel button location.
 *  @param loc enum.
 */
- (void)setCancelButtonLocation:(CancelBtnLoc)loc;

@end

