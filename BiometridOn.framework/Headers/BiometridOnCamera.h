//
//  BiometridOnCamera.h
//  BiometridOn
//
//  Created by Tiago Carvalho on 26/01/2018.
//  Copyright © 2018 Tiago Carvalho. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
#import "BiometridOnEnums.h"

@interface BiometridOnCamera : NSObject

@property StepType step;
@property Orientation orientation;
@property UIView *view;

/*!
 *  @discussion Start camera for step.
 *  @param step BiometridOn StepType, according to step opens the corresponding camera.
 *  @param view UIView where camera is launched.
 */
+ (instancetype)startWithStep:(StepType)step inView:(UIView*)view;

/*!
 *  @discussion Start camera with orientation.
 *  @param orientation BiometridOn Orientation, FRONT or BACK.
 *  @param view UIView where camera is launched.
 */
+ (instancetype)startWithOrientation:(Orientation)orientation inView:(UIView*)view;

/*!
 *  @discussion Take a photo.
 *  @param delegate AVCapturePhotoCaptureDelegate delegate.
 */
-(void)takePhotoWithDelegate:(id<AVCapturePhotoCaptureDelegate>)delegate;

/*!
 *  @discussion Stop camera.
 */
-(void)stop;


@end
