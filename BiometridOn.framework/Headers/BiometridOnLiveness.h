//
//  BiometridOnLiveness.h
//  BiometridOn
//
//  Created by André Santana on 02/04/2019.
//  Copyright © 2019 Tiago Carvalho. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BiometridOnEnums.h"
#import "BiometridOnLivenessCustomization.h"

@interface BiometridOnLiveness : NSObject
    
+ (instancetype)sharedInstance;

/*!
 *  @discussion Face engine.
 *  @param customLayout of the Controller.
 *  @param livenessToken Liveness Token identifier.
 *  @param publicKey Public key identifier.
 *  @param process Process id of the BiometridOn process.
 *  @param viewController View controller where the face liveness engine is going to be launched.
 */
- (void)startLiveness:(BiometridOnLivenessCustomization*)customLayout withLivenessToken:(NSString*)livenessToken withPublicKey:(NSString*)publicKey forProcess:(NSString*)process inViewController:(UIViewController*)viewController andCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Face engine.
 *  @param customLayout of the Controller.
 *  @param livenessToken Liveness Token identifier.
 *  @param publicKey Public key identifier.
 *  @param image1 Front image key identifier.
 *  @param image2 Back image key identifier.
 *  @param process Process id of the BiometridOn process.
 *  @param viewController View controller where the face liveness engine is going to be launched.
 */
- (void)startLivenessWithOcr:(BiometridOnLivenessCustomization*)customLayout withLivenessToken:(NSString*)livenessToken withPublicKey:(NSString*)publicKey withCardImage1:(NSString*)image1 withCardImage2:(NSString*)image2 forProcess:(NSString*)process inViewController:(UIViewController*)viewController andCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;

@end

