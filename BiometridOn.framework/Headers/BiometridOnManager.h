//
//  BiometridOnManager.h
//  BiometridOn
//
//  Created by Tiago Carvalho on 18/01/2018.
//  Copyright © 2018 Tiago Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BiometridOnEnums.h"

@interface BiometridOnManager : NSObject

+ (instancetype)sharedInstance;

/*!
 *  @discussion Creates a BiometridOn process.
 */
- (void)createProcessWithCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Get the current step of a process.
 *  @param process Process id of the BiometridOn process.
 */
- (void)getCurrentStepForProcess:(NSString *)process andCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Get the previous step of a process, updating the current step to this previous one.
 *  @param process Process id of the BiometridOn process.
 */
- (void)getPreviousStepForProcess:(NSString *)process andCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Get the remaining steps of a process.
 *  @param process Process id of the BiometridOn process.
 */
- (void)getRemainingStepsForProcess:(NSString *)process andCallback:(void(^)(NSMutableDictionary *response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Get the completed steps of a process.
 *  @param process Process id of the BiometridOn process.
 */
- (void)getCompletedStepsForProcess:(NSString *)process andCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Get the data of a process.
 *  @param process Process id of the BiometridOn process.
 */
- (void)getProcessData:(NSString *)process andCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Submit a form.
 *  @param formData Form data as a NSDictionary with keys as required.
 *  @param process Process id of the BiometridOn process.
 */
- (void)submitForm:(NSDictionary *)formData forProcess:(NSString *)process andCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Submit a form with files.
 *  @param formData Form data as a NSDictionary with keys as required.
 *  @param process Process id of the BiometridOn process.
 */
- (void)submitForm:(NSDictionary*)formData withFiles:(NSDictionary*)files forProcess:(NSString *)process andCallback:(void(^)(NSDictionary* response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Submit a file.
 *  @param filePath File path of the required file.
 *  @param process Process id of the BiometridOn process.
 */
- (void)submitFile:(NSString *)filePath forProcess:(NSString *)process andCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Submit multiple file.
 *  @param files File paths of the required files with the alias as key.
 *  @param process Process id of the BiometridOn process.
 */
- (void)submitMultiFiles:(NSDictionary *)files forProcess:(NSString *)process andCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Submit an ID card.
 *  @param frontSidePath File path of the front side of the ID card.
 *  @param backSidePath File path of the back side of the ID card.
 *  @param process Process id of the BiometridOn process.
 */
- (void)submitIdCardFrontSide:(NSString *)frontSidePath andBackSide:(NSString *)backSidePath forProcess:(NSString *)process andCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Start video conference.
 *  @param process Process id of the BiometridOn process.
 */
- (void)startVideoConferenceForProcess:(NSString *)process andCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;

/*!
 *  @discussion Delete process.
 *  @param process Process id of the BiometridOn process.
 */
- (void)deleteProcess:(NSString *)process andCallback:(void(^)(NSDictionary *response, StepType step, NSArray *errors))completion;


@end
