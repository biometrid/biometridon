//
//  BiometridOnSDK.h
//  BiometridOn
//
//  Created by Tiago Carvalho on 17/01/2018.
//  Copyright © 2018 Tiago Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BiometridOnSDK : NSObject

+ (instancetype)sharedInstance;

/*!
 *  @discussion BiometridOn SDK initialization with credentials.
 *  @param key Application client key.
 *  @param secret Application client secret.
 *  @param flowId Application flow identifier.
 *  @param serverUrl Application server url.
 *  @param isPusherOn Option to init with Pusher or not.
 *  @warning init must be called at least once by the application before invoking any SDK operations.
 */
- (void)initWithKey:(NSString *)key withSecret:(NSString *)secret withFlowId:(NSString *)flowId withServerUrl:(NSString *)serverUrl andPusherOption:(BOOL)isPusherOn andCallback:(void(^)(NSDictionary* response, NSError *error))completion;

/*!
*  @discussion BiometridOn SDK initialization with credentials.
*  @param key Application client key.
*  @param secret Application client secret.
*  @param flowId Application flow identifier.
*  @param serverUrl Application server url.
*  @param isPusherOn Option to init with Pusher or not.
*  @param map Dictionary to add new values to request's.
*  @warning init must be called at least once by the application before invoking any SDK operations.
*/
- (void)initWithKey:(NSString *)key withSecret:(NSString *)secret withFlowId:(NSString *)flowId withServerUrl:(NSString *)serverUrl withPusherOption:(BOOL)isPusherOn andMapDictionary:(NSDictionary *)map andCallback:(void(^)(NSDictionary* response, NSError *error))completion;


@end
