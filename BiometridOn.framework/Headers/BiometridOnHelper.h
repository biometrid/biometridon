//
//  BiometridOnHelper.h
//  BiometridOn
//
//  Created by Tiago Carvalho on 08/03/2018.
//  Copyright © 2018 Tiago Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BiometridOnHelper : NSObject

+ (BiometridOnHelper *)sharedInstance;

/*!
 *  @discussion Save a file and return its path from an image data, also formats the image with the required settings.
 *  @param photoData Captured photo data.
 *  @return File path where photo is saved.
 */
- (NSString *)saveFileFromData:(NSData *)photoData;

@end
