# BiometridON
BiometridOn is a service-based onboarding solution. It facilitates the process of creating and validating a digital identity of a new (or existing) customer, allowing for remote user identification.

# Installation
BiometridAuth is available through CocoaPods. To install it, simply add the following line to your Podfile:
pod 'BiometridON'

# Documentation
https://polygon.gitbook.io/biometrid-on-ios-sdk/

# License
BiometridAuth is available under the MIT license. See the LICENSE file for more info.
