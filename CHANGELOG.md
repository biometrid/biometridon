# Change Log

## 1.0.3 (17/06/2019)
#### Added:
- StartLivenessWithOcr

## 1.0.2 (15/05/2019)
#### Fixed:
- BiometridOnLivenessCustomization will never pass null values
#### Added:
- Init with a new parameter (NSDictionary)
- This new NSDictionary will add key and value to Headers request’s

## 1.0.1 (06/05/2019)
#### Added:
- BiometridOnLivenessCustomization object
